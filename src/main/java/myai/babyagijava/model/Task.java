package myai.babyagijava.model;

import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
/**
 * This class represents an AI task
 */
@Entity
public class Task{
	@Id
	@GeneratedValue
	protected Long id = null;
	
	@Basic
	@Column(name = "C_TASK_ID")
	protected int taskId;
	
	@Basic
	protected String objective;
	
	@Basic
	@Column(name = "C_TASK_Name")
	protected String taskName;
	
	@OneToMany(mappedBy = "task")
	protected List<CompletedTaskResult> resultElements;
	
	public Task() {
		super();
	}

	public Task(int taskId, String objective, String taskName) {
		super();
		this.taskId = taskId;
		this.objective = objective;
		this.taskName = taskName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getObjective() {
		return objective;
	}

	public void setObjective(String objective) {
		this.objective = objective;
	}

	public int getTaskId() {
		return taskId;
	}

	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public List<CompletedTaskResult> getResultElements() {
		return resultElements;
	}

	public void setResultElements(List<CompletedTaskResult> resultElements) {
		this.resultElements = resultElements;
	}
}
