package myai.babyagijava.model;

/**
 * A lightweight object used by API show the name of a  completed task and its result
 */
public class CompletedTaskInfo {
	public String taskName;
	public String objective;
	public String reslut;
	public CompletedTaskInfo(String taskName, String objective, String reslut) {
		super();
		this.objective = objective;
		this.taskName = taskName;
		this.reslut = reslut;
	}
}
