package myai.babyagijava.model;

/**
 * This class captures an original result from AI and its enriched version
 */
public class EnrichedResult {
	//An original result from AI
	private String result;
	
	//Enriched result
	private String data;
	
	/**
	 * TODO: Implement a way to enrich the result
	 * @param result A response from AI as the result of executing a task
	 */
	public EnrichedResult(String result) {
		super();
		this.result = result;
		this.enrich(result);
	}

	/*
	 * default: no action for enriching the result
	 */
	public void enrich(String result) {
		this.data = result;
	}
	
	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

}
