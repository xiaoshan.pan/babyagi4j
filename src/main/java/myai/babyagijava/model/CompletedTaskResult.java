package myai.babyagijava.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * This class captures a completed task and its result.
 */
@Entity
public class CompletedTaskResult {
	@Id
	@GeneratedValue
	protected Long id = null;

	@Basic
	protected String objective;
	
	@ManyToOne
	@JoinColumn(name = "C_TASK_ID", referencedColumnName = "ID")
	protected Task task;
	
	@Column(columnDefinition = "TEXT")
	protected String result;
	
	@Basic
	@Column(name = "C_RESULT_ID")
	protected String resultId;

	
	public CompletedTaskResult() {
		super();
	}

	public CompletedTaskResult(String objective, Task task, String result, String resultId) {
		this.objective = objective;
		this.task = task;
		this.result = result;
		this.resultId = resultId;
	}

	public String getObjective() {
		return objective;
	}

	public void setObjective(String objective) {
		this.objective = objective;
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getResultId() {
		return resultId;
	}

	public void setResultId(String resultId) {
		this.resultId = resultId;
	}

}
