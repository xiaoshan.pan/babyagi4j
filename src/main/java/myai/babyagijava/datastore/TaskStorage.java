package myai.babyagijava.datastore;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.springframework.stereotype.Component;

import myai.babyagijava.model.Task;
/**
 * An in-memory store (simple queue) for un-completed tasks. 
 */
@Component
public class TaskStorage {
	private Queue<Task> queue = new LinkedList<>();
	private int idCounter = 0;

	public TaskStorage() {
		super();
	}

	public void append(Task t) {
		queue.add(t);
	}

	public int size() {
		return queue.size();
	}
	
	public Task popLeft() {
		return queue.poll();
	}

	public Boolean isEmpty() {
		return queue.isEmpty();
	}

	public int nextTaskId() {
		idCounter++;
		return idCounter;
	}
	
	/**
	 * Fetch all tasks in store.
	 * @return A list of tasks
	 */
	public List<Task> getTasks(){
		List<Task> returnVal = new ArrayList<>();
		
		Object [] obs = queue.toArray();
		for(Object ob : obs) {
			returnVal.add((Task) ob);
		}
		
		return returnVal;
	}
	
	/**
	 * Fetch names of all task.
	 * @return A list containing all names of the tasks in the store
	 */
	public List<String> getTaskNames (){
		Object[] lObs = queue.toArray();
		
		List<String> returnVal = new ArrayList<String>();
		for(Object o : lObs) {
			Task t = (Task)o;
			returnVal.add(t.getTaskName());
		}
		
		return returnVal;
	}
	
	/**
	 * Replace all existing tasks with a new list
	 * @param tasks A list of new tasks
	 */
	public void replace(List<Task> tasks) {
		queue.clear();
		queue.addAll(tasks);
	}
}
