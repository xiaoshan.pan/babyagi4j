package myai.babyagijava.datastore;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import myai.babyagijava.model.CompletedTaskResult;
import myai.babyagijava.model.Task;
import myai.babyagijava.persistence.DataAccessService;

/**
 * A result storage as either an ArrayList in memory or a DB, determined by
 * environment variable {@link myai.babyagijava.configuration.AppConfiguration#ENABLE_DB_STORAGE}.
 */
@Component
public class ResultStorage {

	private static final Logger logger = LoggerFactory.getLogger(ResultStorage.class);
	private List<CompletedTaskResult> resultStore = new ArrayList<CompletedTaskResult>();

	public ResultStorage() {
		super();
	}

	/**
	 * Add a result to store.
	 * @param objective Agent objective
	 * @param t Task
	 * @param result Result of the task
	 * @param resultId ID of the result
	 * @param useDB A flag to decide if a database will be used as store
	 * @param das A reference to DataAccessService bean
	 */
	public void add(String objective, Task t, String result, String resultId, boolean useDB, DataAccessService das) {

		if (useDB) {
			Task savedT = das.saveTask(t);
			CompletedTaskResult e = new CompletedTaskResult(objective, savedT, result, resultId);
			das.saveResultElement(e);
		} else {
			resultStore.add(new CompletedTaskResult(objective, t, result, resultId));
		}
	}

	/**
	 * A method to print out all task names in store.
	 * @param useDB A flag to decide if a database will be used as store
	 * @param das A reference to DataAccessService bean
	 */
	public void printResults(boolean useDB, DataAccessService das) {
		List<CompletedTaskResult> allObjs = new ArrayList<>();
		if (useDB) {
			allObjs = das.findAllResultElement();
		} else {
			allObjs = resultStore;
		}

		logger.info("Result Store contains " + allObjs.size() + " completed tasks.");
		String results = "";
		for (CompletedTaskResult e : allObjs) {
			results += " - " + e.getTask().getTaskName() + "\n";
		}
		logger.info("\n" + results);
	}
 
	/**
	 * A method to get the top n completed tasks for the objective.
	 * @param objective Agent objective
	 * @param topResultsNum The number of results to return
	 * @param useDB A flag to decide if a database will be used as store
	 * @param das A reference to DataAccessService bean
	 * @return A string containing a list of task names
	 */
	public String query(String objective, int topResultsNum, boolean useDB, DataAccessService das) {
		// find all tasks associated with the objective
		List<CompletedTaskResult> firstList = new ArrayList<>();

		if (useDB) {
			firstList = das.findResultElementByObjective(objective);

		} else {

			for (CompletedTaskResult e : resultStore) {
				if (e.getObjective().equalsIgnoreCase(objective)) {
					firstList.add(e);
				}
			}
		}
		// identify the top n tasks and return their results
		if (firstList.isEmpty()) {
			return null;
		} else {
			List<String> secondList = new ArrayList<>();
			int counter = 1;
			for (CompletedTaskResult e : firstList) {
				secondList.add(e.getTask().getTaskName());
				counter++;
				if (counter >= topResultsNum) {
					break;
				}
			}

			if (secondList.isEmpty()) {
				return null;
			} else {
				String returnVal = "";
				for (String s : secondList) {
					returnVal = returnVal + s + "\n";
				}
				return returnVal;
			}
		}
	}
}
