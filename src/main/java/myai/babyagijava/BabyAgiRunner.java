package myai.babyagijava;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import myai.babyagijava.apiclient.OpenAiClient;
import myai.babyagijava.configuration.AppConfiguration;
import myai.babyagijava.datastore.ResultStorage;
import myai.babyagijava.datastore.TaskStorage;
import myai.babyagijava.model.EnrichedResult;
import myai.babyagijava.model.Task;
import myai.babyagijava.persistence.DataAccessService;

/**
 * This class implements a Baby AGI Loop as a Spring Boot CommandLineRunner.
 */
@Component
public class BabyAgiRunner implements CommandLineRunner {
	private static final Logger logger = LoggerFactory.getLogger(BabyAgiRunner.class);

	@Autowired
	AppConfiguration config;

	@Autowired
	DataAccessService das;

	@Autowired
	OpenAiClient openAiClient;

	@Autowired
	public TaskStorage taskStorage;

	@Autowired
	public ResultStorage resultStorage;

	private final int TOP_RESULTS_NUM = 5;

	@Override
	public void run(String... args) throws Exception {

		if (AppConfiguration.OPENAI_API_KEY == null) {
			logger.info("OPENAI_API_KEY environment variable is missing.");
			System.exit(0);
		}

		logger.info("StartupRuner is running with LLM: " + config.LLM_MODEL + " and MAX_TOKENS: " + config.MAX_TOKENS);

		// Run Baby AGI Loop
		runBabyagiLoop();
	}

	/**
	 * This method runs the BabyAGI loop.
	 * 
	 * @throws InterruptedException
	 */
	private void runBabyagiLoop() throws InterruptedException {
		// For a human to step through each iteration
		Scanner in = new Scanner(System.in);
		String choose;

		// Log objective
		logger.info("Objective: " + config.OBJECTIVE);

		// Create initial task and add it to the storage queue
		Task initialTask = new Task(taskStorage.nextTaskId(), config.OBJECTIVE, config.INITIAL_TASK);
		taskStorage.append(initialTask);

		while (!taskStorage.isEmpty()) {
			// Log the latest task list
			logger.debug("*****LATEST TASK LIST*****");
			int index = 1;
			for (String taskName : taskStorage.getTaskNames()) {
				logger.debug(index + ". " + taskName + ".");
				index++;
			}

			logger.info("*****COMPLETED TASKS*****");
			resultStorage.printResults(config.ENABLE_DB_STORAGE, das);

			// For a human to decide either continue or exit the loop
			System.out.println("Continue the program Yes or N?");
			choose = in.next();
			if (choose.equalsIgnoreCase("N")) {
				in.close();
				System.exit(0);
			}

			// Pull the first incomplete task
			Task t = taskStorage.popLeft();

			logger.info("*****NEXT TASK*****");
			logger.info(t.getTaskName());

			// Send task to execution agent to complete the task based on the context
			String result = executionAgent(config.OBJECTIVE, t.getTaskName());
			logger.debug("*****TASK RESULT*****");
			logger.debug("\n" + result);

			// Enrich result and store it in a result storage
			EnrichedResult enrichedResult = new EnrichedResult(result);
			String resultId = "result_" + t.getTaskId();
			resultStorage.add(config.OBJECTIVE, t, result, resultId, config.ENABLE_DB_STORAGE, das);

			// Create new tasks and re-prioritize task list
			List<Task> newTaskList = taskCreationAgent(config.OBJECTIVE, enrichedResult, t.getTaskName(),
					taskStorage.getTaskNames());

			logger.info("Adding new tasks to task_storage");
			logger.info("Before: " + taskStorage.size() + " tasks.");
			logger.info("Adding " + newTaskList.size() + " more.");
			for (Task newTask : newTaskList) {
				taskStorage.append(newTask);
			}
			logger.info("After : " + taskStorage.size() + " tasks.");

			// Prioritize all tasks
			List<Task> prioritizedTasks = prioritizationAgent();

			if (!prioritizedTasks.isEmpty()) {
				taskStorage.replace(prioritizedTasks);
			}

			// Sleep a bit before checking the task list again
			Thread.sleep(5000);
		}
	}

	/**
	 * An agent that re-prioritizes the existing task list where higher-priority
	 * tasks are those that act as pre-requisites or are more essential for meeting
	 * the objective.
	 * 
	 * @return a list of re-prioritized tasks
	 */

	private List<Task> prioritizationAgent() {
		List<String> taskNames = taskStorage.getTaskNames();

		// Build prompt
		String prompt = "You are tasked with prioritizing the following tasks: \n";
		int counter = 1;
		for (String s : taskNames) {
			prompt += counter + ". " + s + ".\n";
			counter++;
		}

		prompt += "Consider the ultimate objective of your team: " + config.OBJECTIVE + ".\n";
		prompt += "Tasks should be sorted from highest to lowest priority, where higher-priority tasks are those that act as pre-requisites or are more essential for meeting the objective.\n";
		prompt += "Do not remove any tasks. Return the ranked tasks as a numbered list in the format:\n";
		prompt += "#. First task\n";
		prompt += "#. Second task\n";
		prompt += "The entries must be consecutively numbered, starting with 1. The number of each entry must be followed by a period.\n";
		prompt += "Do not include any headers before your ranked list or follow your list with any other output.\n";

		logger.debug("*****TASK PRIORITIZATION AGENT PROMPT****");
		logger.debug("\n" + prompt);

		String response = openAiClient.call(prompt);
		logger.info("*****TASK PRIORITIZATION AGENT RESPONSE****");
		logger.info("\n" + response);

		List<Task> returnVal = new ArrayList<>();
		if (response == null || response.isEmpty()) {
			logger.debug("Received empty response from priotritization agent. Keeping task list unchanged.*");
		} else {
			returnVal = convertResponseToTasks(response);
		}
		return returnVal;
	}

	/**
	 * An agent that uses the result from an execution agent to create new tasks to
	 * fulfill objective.
	 * 
	 * @param objective      Objective of agent
	 * @param enrichedResult Enriched result of a task
	 * @param taskName       Name of the task
	 * @param taskList       Uncompleted tasks
	 * @return A list of new tasks
	 */
	private List<Task> taskCreationAgent(String objective, EnrichedResult enrichedResult, String taskName,
			List<String> taskList) {
		// Build prompt
		String prompt = "You are to use the result from an execution agent to create new tasks with the following objective: "
				+ objective + ". ";
		prompt += "The last completed task has the result: \n" + enrichedResult.getData() + "\n";
		prompt += "This result was based on this task description: " + taskName + ".\n";

		if (!taskList.isEmpty()) {
			prompt += "These are incomplete tasks: ";
			for (String t : taskList) {
				prompt += t + ", ";
			}
		}

		prompt += "Based on the result, return a list of tasks to be completed in order to meet the objective. ";

		if (!taskList.isEmpty()) {
			prompt += "These new tasks must not overlap with incomplete tasks. ";
		}

		prompt += "Return one task per line in your response. The result must be a numbered list in the format: \n";
		prompt += "#. First task\n";
		prompt += "#. Second task\n";
		prompt += "The number of each entry must be followed by a period. If your list is empty, write \"There are no tasks to add at this time.\"\n";
		prompt += "Unless your list is empty, do not include any headers before your numbered list or follow your numbered list with any other output.\n";

		logger.debug("*****TASK CREATION AGENT PROMPT****");
		logger.debug("\n" + prompt);

		String response = openAiClient.call(prompt);
		logger.info("****TASK CREATION AGENT RESPONSE****");
		logger.info("\n" + response);

		List<Task> returnVal = new ArrayList<>();
		if (response == null || response.isEmpty()) {
			logger.debug("Received empty response from priotritization agent. Keeping task list unchanged.*");
		} else {
			returnVal = convertResponseToTasks(response);
		}

		return returnVal;
	}

	/**
	 * An agent that performs a task based on the given objective, considering
	 * previously completed tasks as the context.
	 * 
	 * @param objective Objective of agent
	 * @param taskName  Task name to be executed
	 * @return Response from AI
	 */
	private String executionAgent(String objective, String taskName) {
		String prompt = "Perform one task based on the following objective: " + objective + ".\n";

		String context = contextAgent(objective, TOP_RESULTS_NUM);

		if (context != null) {
			prompt = prompt + "Take into account these previously completed tasks:\n" + context;
		}

		prompt = prompt + "\nYour task: " + taskName + "\nResponse:";
		logger.info("****TASK EXECUTION AGENT PROMPT****");
		logger.info(prompt);

		String response = openAiClient.call(prompt);
		logger.info("****TASK EXECUTION AGENT RESPONSE****");
		logger.info("\n" + response);

		return response;
	}

	/**
	 * An agent that gets the top n completed task results for the objective. Those
	 * are used by Execution Agent as context.
	 * 
	 * @param query         Objective of agent
	 * @param topResultsNum Number of results to consider as context
	 * @return A string that contains a list of completed tasks (their names)
	 */
	private String contextAgent(String query, int topResultsNum) {
		return resultStorage.query(query, topResultsNum, config.ENABLE_DB_STORAGE, das);
	}

	/**
	 * A helper function that converts an AI response to a number of tasks.
	 * 
	 * @param response AI response
	 * @return A list of tasks
	 */
	private List<Task> convertResponseToTasks(String response) {
		List<Task> returnVal = new ArrayList<>();

		// Remove headers, if any
		response = response.substring(response.indexOf("1. "), response.length());

		String[] newTasks = response.split("\n");

		// Process each line
		for (String s : newTasks) {
			// Drop it if the string is empty, no ". " element, or is not a numbered
			if (s.isBlank() || !s.contains(". ") || !s.matches(".*\\d.*")) {
				continue;
			}

			String newStr = "";
			if (s.endsWith(".")) {
				newStr = s.substring(s.indexOf(". ") + 2, s.length() - 1);
			} else {
				newStr = s.substring(s.indexOf(". ") + 2, s.length());
			}
			returnVal.add(new Task(taskStorage.nextTaskId(), config.OBJECTIVE, newStr));
		}
		return returnVal;
	}

}
