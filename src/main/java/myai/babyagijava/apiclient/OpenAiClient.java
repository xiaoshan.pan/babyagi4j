package myai.babyagijava.apiclient;

import static dev.langchain4j.data.message.UserMessage.userMessage;

import dev.langchain4j.memory.ChatMemory;
import dev.langchain4j.memory.chat.TokenWindowChatMemory;
import dev.langchain4j.model.chat.ChatLanguageModel;
import dev.langchain4j.model.openai.OpenAiChatModel;
import dev.langchain4j.model.openai.OpenAiTokenizer;
import myai.babyagijava.configuration.AppConfiguration;
/**
 * OpenAI API client
 */
public class OpenAiClient {

	private ChatLanguageModel model;
	private ChatMemory chatMemory;
	
	/**
	 * Constructor
	 */
	public OpenAiClient(int maxToken, String openAiKey, String llm ) {
		model = OpenAiChatModel.withApiKey(AppConfiguration.OPENAI_API_KEY);
		chatMemory = TokenWindowChatMemory.withMaxTokens(maxToken, new OpenAiTokenizer(llm) );
	}
	
	/**
	 * This method makes a call to OpenAI API
	 * @param prompt LLM prompt sent to LLM API
	 * @return Response received from LLM API
	 */
	public String call(String prompt) {
		chatMemory.add(userMessage(prompt));
		return model.generate(chatMemory.messages()).content().text();
	}
}
