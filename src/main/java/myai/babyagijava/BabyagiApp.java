package myai.babyagijava;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
/**
 * Spring Boot application.
 */
@SpringBootApplication
@ComponentScan("myai.babyagijava")
public class BabyagiApp {
	private static final Logger logger = LoggerFactory.getLogger(BabyagiApp.class);

	public static void main(String[] args) {
		SpringApplication.run(BabyagiApp.class, args);
		logger.info("BabyAGI App is running...");
	}
}
