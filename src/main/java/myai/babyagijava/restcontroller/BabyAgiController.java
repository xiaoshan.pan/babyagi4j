package myai.babyagijava.restcontroller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import myai.babyagijava.datastore.TaskStorage;
import myai.babyagijava.model.CompletedTaskInfo;
import myai.babyagijava.model.CompletedTaskResult;
import myai.babyagijava.model.Task;
import myai.babyagijava.persistence.DataAccessService;

@RestController
@RequestMapping("/babyagi")
@Api(value = "Swagger API to observe tasks and their result as AI agents work on them in a loop.")
public class BabyAgiController {
	private static final Logger logger = LoggerFactory.getLogger(BabyAgiController.class);

	@Autowired
	DataAccessService das;

	@Autowired
	public TaskStorage taskStorage;

	@ApiOperation(value = "Fetch all completed task and result", response = List.class)
	@RequestMapping(value = "/completedtaskresults", method = RequestMethod.GET)
	public List<CompletedTaskInfo> getRCompletedTaskResults() {
		logger.debug("Fetch all completed task and result.");

		List<CompletedTaskInfo> returnVal = new ArrayList<>();
		for (CompletedTaskResult tr : das.findAllResultElement()) {
			returnVal.add(new CompletedTaskInfo(tr.getTask().getTaskName(), tr.getObjective(), tr.getResult()));
		}
		return returnVal;
	}

	@ApiOperation(value = "Fetch all incompleted tasks", response = List.class)
	@RequestMapping(value = "/tasks", method = RequestMethod.GET)
	public List<Task> getTasks() {
		return taskStorage.getTasks();
	}
}
