package myai.babyagijava.persistence.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import myai.babyagijava.model.CompletedTaskResult;
/**
 * Repository for {code CompletedTaskResult}
 */
public interface CompletedTaskResultRepository extends PagingAndSortingRepository<CompletedTaskResult, Long> {
	List<CompletedTaskResult> findByObjective(String objective);
}
