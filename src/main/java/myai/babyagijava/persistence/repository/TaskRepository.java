package myai.babyagijava.persistence.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import myai.babyagijava.model.Task;

/**
 * Repository for {code Task}
 */
public interface TaskRepository extends PagingAndSortingRepository<Task, Long> {}
