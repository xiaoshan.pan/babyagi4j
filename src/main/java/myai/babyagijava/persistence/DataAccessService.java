package myai.babyagijava.persistence;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;

import myai.babyagijava.model.CompletedTaskResult;
import myai.babyagijava.model.Task;
import myai.babyagijava.persistence.repository.CompletedTaskResultRepository;
import myai.babyagijava.persistence.repository.TaskRepository;

/**
 * An API service to access database
 */
@Component
public class DataAccessService {
	private static Logger logger = LoggerFactory.getLogger(DataAccessService.class);

	@Autowired
	private CompletedTaskResultRepository rer;
	
	@Autowired
	private TaskRepository tr;
	
	/***********************/
	/* Task */
	public Task saveTask(Task t) {
		return tr.save(t);
	}
	
	/***********************/
	/* ResultElement */
	public CompletedTaskResult saveResultElement(CompletedTaskResult e) {
		logger.debug("saving ResultElement " + e);
		return rer.save(e);
	}
	
	public long countAllResultElement() {
		return rer.count();
	}
	
	public List<CompletedTaskResult> findAllResultElement(){
		return Lists.newArrayList(rer.findAll());
	}
	
	public List<CompletedTaskResult> findResultElementByObjective(String objective){
		return rer.findByObjective(objective);
	}
}
