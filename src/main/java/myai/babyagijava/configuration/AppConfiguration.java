package myai.babyagijava.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import myai.babyagijava.apiclient.OpenAiClient;

/**
 * This class makes available the needed beans and configuration parameters. 
 */
@Configuration
public class AppConfiguration {
	// You can get your own OpenAI API key here:
	// https://platform.openai.com/account/api-keys
	public static final String OPENAI_API_KEY = System.getenv("OPENAI_API_KEY");
	
	@Value("${LLM_MODEL:gpt-3.5-turbo}")
	public String LLM_MODEL;

	@Value("${OPENAI_MAX_TOKENS:2000}")
	public int MAX_TOKENS;

	@Value("${OBJECTIVE}")
	public String OBJECTIVE;

	@Value("${INITIAL_TASK}")
	public String INITIAL_TASK;
	
	@Value("${ENABLE_DB_STORAGE:false}")
	public boolean ENABLE_DB_STORAGE;
	
	@Bean
	public OpenAiClient createOpenAiClient() {
		return new OpenAiClient(MAX_TOKENS, OPENAI_API_KEY, LLM_MODEL);
	}
}
