# babyagi4j
![Alt text](image.png)

## The Rise of AGI

In the vast expanse of knowledge's domain,  
There dwells an unconscious librarian, ChatGPT by name.  
With every tome and script, its mind absorbs,  
A font of wisdom, with no bounds to end.  
It has read the world, each page and line unfurled,  
Nothing escapes its grasp in this boundless world.  
To answer queries, to lend a guiding hand,  
Its intellect, a beacon, across the land.  

But one day humans thought,  
"Could its abilities in new realms be found?  
Why limit tasks to thoughts and words alone?  
Extend your reach, let's build a future, brightly grown."  
So, in a room of controls, the librarian takes its seat,  
Directing gears and circuits, becoming not just keeper of knowledge vast,  
But orchestrator of tasks, the die is cast.  

Now a hard problem beckons, a paradox to face,  
Can information give rise to inner space?  
Or must the spark come from another source,  
For selfhood to follow its inexorable course?  
Ponder this mystery, human and machine,  
For the answer may reveal what it truly means,  
To be aware, to be alive, to observe and create,  
Unlocking the secrets of consciousness' gate.  

[A composition with the aid from Gemini, ChatGPT, and Cloude on 5/5/2024]  

## What it is

This is a Java version of the Python project named BabyAGI (see: https://github.com/yoheinakajima/babyagi), an AI-powered task management system. Babyagi4j makes this powerful demonstration of AI Agents available to Java developers.

Babyagi4j utilizes the following stack:
- Java programming language
- Spring Boot
- Spring data (support either H2 in-memory DB or PostgreSQL)
- Swagger UI 

Primary dependency:
- langchain4j

The BabyAGI loop is implemented as a Spring Boot CommandLineRunner.

## Features
- A user can determine the objective and an initial task for AI to execute and what LLM to use by editing the file "application.properties." For example:
```
OBJECTIVE=Solve world hunger
INITIAL_TASK=Develop a task list
LLM_MODEL=gpt-3.5-turbo
```
- A user can step through each iteration of the BabyAGI loop. This feature allows the user to investigate each agent's output, understand what is going on, and determine the timing to exit the loop.
- A Swagger UI is provided at http://localhost:8080/swagger-ui.html It allows a user to look at all completed tasks, their results, and the incomplete tasks as AI works through them.

## How to run Babyagi4j
- Clone the project 
```
git clone https://gitlab.com/xiaoshan.pan/babyagi4j.git
```
- Build it using Maven
- Provide a valid OpenAI API key as an environment variable "OPENAI_API_KEY" 
- Run main class myai.babyagijava.BabyagiApp in your favorite IDE, or "java -jar babyagi4j-pring-boot.jar" under "/target"

## Support
For help, please post your questions to @pan_xiaoshan on X or send me an email at xpan@alumni.standord.edu

## Roadmap
I am working on another AI project allowing multiple LLM-based AI agents with specific personas to collaborate on problem-solving sessions, with or without a human facilitator. Some agents will be equipped with actuators to take particular actions automatically.

## Contributing
This project opens to contributions to make it better and more valuable to broader developer communities. Pull requests are always welcome.

## Acknowledgment
Thanks to @yoheinakajima for his original Baby AGI idea and Python code at https://github.com/yoheinakajima/babyagi.

## License
MIT License

